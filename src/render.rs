pub fn render(map: [bool; 4900], enemies: [char; 4900]) {
    let mut render_map: Vec<char> = Vec::new();
    let mut i = 0;
    for c in enemies.iter() {
        if i % 100 == 0 {
            render_map.push('\n');
        }
        if map[i] {
            render_map.push('#');
        } else {
            render_map.push(*c);
        }
        i += 1;
    }
    let render_map_string: String = render_map.into_iter().collect();
    println!("{}", render_map_string);
}
