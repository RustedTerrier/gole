use std::fs;

pub mod towers;
pub mod render;

fn main() {
    let mut tower1 = new_tower("Orchestra", 0, 0);
    let range = tower1.set_stats();
    println!("Range (\n    width:  {}\n    startx: {}\n    starty: {}\n)", range.width, range.startx, range.starty);
    render::render(get_map(), thing())
}

fn thing() -> [char; 4900] {
    let mut array = [' '; 4900];
    let mut counter = 0;
    for i in array.iter_mut() {
        if counter % 67 == 0 {
            *i = '.';
        }
        counter += 1;
    }
    array
}

fn get_map() -> [bool; 4900] {
    let visual_map_string: String = fs::read_to_string("src/maps/map_one").unwrap().split('\n').collect();
    let visual_map: Vec<char> = visual_map_string.chars().collect();
    let mut map = [false; 4900];
    let mut j = 0;
    for i in visual_map {
        match i {
            | ' ' => map[j] = false,
            | _ => map[j] = true
        }
        j += 1;
    }
    map
}

pub fn new_tower(style_str: &str, xpos: u16, ypos: u16) -> towers::Tower {
    let style;
    let tier;
    let branch;
    match style_str {
        | "Scouting Tower" => {
            style = 0;
            tier = 0;
            branch = 0;
        },
        | "Outpost" => {
            style = 0;
            tier = 1;
            branch = 0;
        },
        | "Steel Outpost" => {
            style = 0;
            tier = 2;
            branch = 0;
        },
        | "Lookout" => {
            // Can send flares
            style = 0;
            tier = 3;
            branch = 0;
        },
        | "Arsonists" => {
            // Set's ground on fire hopefully
            style = 0;
            tier = 4;
            branch = 0;
        },
        | "The Swift Longbows" => {
            // Very fast
            style = 0;
            tier = 3;
            branch = 1;
        },
        | "The Electric Bowmen" => {
            // Shoots lightning
            style = 0;
            tier = 4;
            branch = 1;
        },
        | "Training Ground" => {
            style = 1;
            tier = 0;
            branch = 0;
        },
        | "Armory" => {
            style = 1;
            tier = 1;
            branch = 0;
        },
        | "Barracks" => {
            style = 1;
            tier = 2;
            branch = 0;
        },
        | "Wood Keep" => {
            // Tanky tree people
            style = 1;
            tier = 3;
            branch = 0;
        },
        | "Doleney Castle" => {
            // Doleney are a form of tree people
            // They are still in the wood keep, but are tankier here and have extra moves, like
            // spreading vines
            style = 1;
            tier = 4;
            branch = 0;
        },
        | "Mermen Castle" => {
            // Can use deadly tridents. Maybe slow people down with waves
            style = 1;
            tier = 3;
            branch = 1;
        },
        | "Shrine to the Kraken" => {
            // When last mermen dies, a tentacle kills all enemy in the vicinity
            // A side note to myself: this is ascii, that's not going to be visible.
            style = 1;
            tier = 4;
            branch = 1;
        },
        | "Magician" => {
            style = 2;
            tier = 0;
            branch = 0;
        },
        | "Wizard" => {
            style = 2;
            tier = 1;
            branch = 0;
        },
        | "Wizard's Keep" => {
            style = 2;
            tier = 2;
            branch = 0;
        },
        | "GYerkersen" => {
            // Sound based magic. Should be faster
            style = 2;
            tier = 3;
            branch = 0;
        },
        | "Orchestra" => {
            // Very long range, very fast damage
            // Like an inverted sniper
            style = 2;
            tier = 4;
            branch = 0;
        },
        | "Order of Paernope" => {
            // Paernope is a rock that allows magic to happen. Here they are experts on it, a least
            // according to my lore
            style = 2;
            tier = 3;
            branch = 1;
        },
        | "Guild of Paernope" => {
            // Master of magic, these wizards can create and use almost any commands, but because
            // of how many they have on their staffs, their speed is slowed, just as their old
            // minds. But make no mistake, against this tower, you stand no chance.
            style = 2;
            tier = 4;
            branch = 1;
        },
        | _ => {
            style = 0;
            tier = 0;
            branch = 0;
        }
    };
    towers::Tower { style, tier, branch, xpos, ypos }
}
