pub struct Tower {
    pub style:  u8, // 0 is archer, 1 is barracks, 2 is wizard
    pub tier:   u8, // 5 tiers
    pub branch: u8, // For when it splits off at the 4th tier.
    // pub radius_square: bool, // Experimental Feature as of now
    pub xpos:   u16,
    pub ypos:   u16
}

pub struct Range {
    pub width:  u16,
    pub startx: i16,
    pub starty: i16
}

impl Tower {
    pub fn set_stats(&mut self) -> Range {
        // Make a struct of the range.
        let mut range = Range { width: 0, startx: 0, starty: 0 };
        // Get the width/height
        range.width = match self.style {
            | 0 => match self.tier {
                | 0 => 10,
                | 1 => 15,
                | 2 => 20,
                | 3 => match self.branch {
                    | 0 => 40,
                    | 1 => 25,
                    | _ => 0
                },
                | 4 => match self.branch {
                    | 0 => 40,
                    | 1 => 25,
                    | _ => 0
                },
                | _ => 0
            },
            | 1 => 10,
            | 2 => match self.tier {
                | 0 => 10,
                | 1 => 10,
                | 2 => 15,
                | 3 => match self.branch {
                    | 0 => 25,
                    | 1 => 20,
                    | _ => 0
                },
                | 4 => match self.branch {
                    | 0 => 45,
                    | 1 => 20,
                    | _ => 0
                },
                | _ => 0
            },
            | _ => 30
        };
        range.startx = (self.xpos - (range.width / 2)) as i16;
        range.starty = (self.ypos - (range.width / 2)) as i16;
        range
    }

    // fn get_enemies_in_range(&mut self, enemies: Vec<u16>, range: Range) {}
}
